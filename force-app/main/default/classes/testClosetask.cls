@istest(seeAllData=true)
public class testClosetask{

    public class MockHttpResponseGenerator implements HttpCalloutMock {
        String jsonResponse;

        public MockHttpResponseGenerator(String jsonResponse) {
            this.jsonResponse = jsonResponse;
        }

        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(this.jsonResponse);
            res.setStatusCode(200);
            res.setStatus('SUCCESS');

            return res;
        }
    }

    static testMethod void unittest() {
        Test.startTest();

        //Profile    
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 

        //User
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, department = 'STP',
                        TimeZoneSidKey='America/Los_Angeles', UserName=Math.random() + '@test.com');

        Insert u;
        lead le = new lead();
        Le.lastname = 'Test';
        Le.Company = 'Abc';
        Le.leadsource = 'Email';
        Le.industry = 'Banking';
        le.country = 'United States';
        le.status = 'Marketing Qualified';
        le.isapproved__c =true;
        le.Type__c = 'Analyst';
        le.Request_New_Owner__c = u.username;
        Insert le;

        Campaign cam  = new Campaign();
        cam.Name = 'Test';
        Insert cam;
        Account Acc = new Account();
        Acc.Name = 'Test1';
        Acc.Primary_Campaign_Source__c = cam.Id;
        Acc.Type = 'Analyst';
        Acc.Industry = 'Banking';
        Acc.BillingCountry = 'United States';
        Acc.BillingCity = 'Acv';
        Acc.BillingStreet = 'xyz';
        Acc.BillingPostalCode = '1234';
        Acc.BillingState = 'Alaska';
        Acc.isapproved__c =true;
        Acc.ShippingCountry= 'United States';
        Acc.ShippingCity= 'Acv';
        Acc.ShippingStreet= 'xyz';
        Acc.ShippingPostalCode = '1234';
        Acc.ShippingState= 'Alaska';
        Acc.Request_New_Owner__c = u.username;
        Insert Acc;

        Opportunity Opp = new Opportunity();
        Opp.Name = 'Test';
        Opp.AccountId = Acc.id;
        Opp.Type = 'New Business';
        Opp.StageName = 'Prospect';
        Opp.CloseDate = System.today(); 
        Opp.isapproved__c =true;
        // Opp.Request_New_Owner__c = u.username;
        Insert Opp; 

        Task ta = new task();
        ta.subject = 'Test1';
        ta.status = 'Not started';
        ta.priority = 'High';
        ta.whoid = le.id;
        insert ta;

        Lead le2 = [Select id, firstname, status from lead where id =: le.id];
        le2.firstname= 'In progress';
        update le2;

        Account Acc2 = [Select name from account where id =: Acc.id];
        Acc2.name = 'test2';
        update Acc2;

        Opportunity Opp2 = [Select id, description from opportunity where id =: Opp.id];
        Opp2.description= 'In progress';
        update Opp2;   

        Licencing__c li = new Licencing__c();
        li.Customer_Name__c = Acc2.id;
        li.Machine_ID__c = 'aa:bb:cc:dd:ee';
        li.Support_Type__c = 'Eval';
        li.Expiry_Date__c = System.today();
        li.Start_Date__c = System.today();
        insert li;

        li.Id = null;
        li.Full_Interfaces__c = true;
        li.Full_Real_Servers__c = true;

        String response = '{"Lic_ALBExpiry":"16 Jan 2030","Lic_ALBPeriod":"16 Jan 2030","Lic_ApplicationExpiry":"16 Jan 2030","Lic_ApplicationPeriod":"16 Jan 2030","Lic_AuthorisationExpiry":"16 Jan 2030","Lic_AuthorisationPeriod":"16 Jan 2030","Lic_B2BExpiry":"16 Jan 2030","Lic_B2BPeriod":"16 Jan 2030","Lic_CacheExpiry":"16 Jan 2030","Lic_CachePeriod":"16 Jan 2030","Lic_CompanyId":"","Lic_ContactPerson":"","Lic_CreatedBy":"","Lic_CreatedDate":"16 Jan 2030","Lic_GlobalExpiry":"16 Jan 2030","Lic_GlobalPeriod":"16 Jan 2030","Lic_InterfacesExpiry":"16 Jan 2030","Lic_InterfacesNumber":5,"Lic_InterfacesPeriod":5,"Lic_InterfacesFull":true}';

        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(response));
        insert li;

        Test.stopTest();
    }
  
}