public class AuthCallout {
   
   @future (callout=true)
   public static void basicAuthCallout(Id LicId){
   String mbody;
   String Lic_ApplicationExpiry;
   String Lic_AuthorisationExpiry;
   String Lic_GlobalExpiry;
   String Lic_GlobalPeriod;
   String Lic_AuthorisationPeriod ;
   String Lic_ApplicationPeriod ;
   String Lic_RealServersExpiry;
   String Lic_LicenceText;
   String Lic_UpdatedDate;
   String Lic_CreatedDate;
   String Lic_UpdatedBy;
   String Lic_RealServersPeriod;
   String Lic_InterfacesPeriod;
   String Lic_B2BPeriod;
   String Lic_IssuedTo;
   String Lic_B2BExpiry;
   String Lic_ALBPeriod;
   String Lic_Telephone;
   String Lic_InterfacesExpiry;
   String Lic_flightPathExpiry;
   String Lic_flightPathPeriod;
   String Lic_layer4Expiry;
   String Lic_layer7Expiry;
   String Lic_RealServersFull;
   String Lic_CreatedBy;
   String Lic_ALBExpiry;
   String Lic_ServerRef;
   String Lic_jetSTREAMFull;
   String Lic_ContactPerson;
   String Lic_CompanyId;
   String Lic_InterfacesFull;
   String Lic_Uuid;
   String Lic_Name;
   String Lic_MachineId;
   String Lic_LicenceID;
   String Lic_IssuedDate;
   String Lic_CacheExpiry;
   String Lic_SSLExpiry;
   String Lic_SSLPeriod;
   String Lic_InterfacesNumber;
   String Lic_ReviewDate;
   String Lic_UserId;
   String Lic_CachePeriod;
   String Lic_RealServersNumber;
   String Lic_jetSTREAMPeriod;
   String Lic_jetSTREAMNumber;
   String Lic_jetSTREAMExpiry;
   String Lic_layer4Period;
   String Lic_layer7Period;
   String Throughput = '';
   String Temp_Throughput = ''; 
   Date B2BExp;
   Date ALBExp;
   Date CacheExp;
   Date flightPathExp;
   Date InterfacesExp;
   Date LExpiryDate;
   Date RealServersExp;
   Date SSLExpiry;
   Date jetSTREAMExpiry;
   Date layer7Expiry;
   Date layer4Expiry;
   Date ApplicationExpiry;
   Date AuthorisationExpiry;
   Date GlobalExpiry;
     Licencing__c l = [Select Machine_ID__c,Customer_Name__r.Name,Contact_Person__r.Name,Contact_Telephone__c,
                             Issue_Date__c,Server_Ref__c,ALB_Base_licence__c,Secure_Socket__c,HTTP_Caching__c,
                             flightPATH__c,Interfaces__c,Real_Servers__c,Full_Interfaces__c,Full_Real_Servers__c,
                             Business_To_Business__c,Temporary_Interfaces_Duration__c,Number_of_Temporary_Interfaces__c,
                             Temporary_Real_Servers_Duration__c,Number_of_Temporary_Real_Servers__c,
                             jetSTREAM_Connections__c,Full_jetSTREAM_Connections__c,Temporary_jetSTREAM_Connections_Duration__c,
                             Number_of_Temporary_jetSTREAM_Connection__c,Global_server_load_balancing__c,Pre_Authentication__c,
                             Application_Firewall__c,Layer4__c,Layer7__c,Full_Throughput__c,Temp_Throughput__c ,Temp_Throughput_Duration__c 
                             ,Throughput__c FROM Licencing__c where ID =: LicId];
    
    
     
     if(l.Server_Ref__c == null){l.Server_Ref__c = '';} 
     if(l.Secure_Socket__c == null){l.Secure_Socket__c = '';} 
     if(l.HTTP_Caching__c == null){l.HTTP_Caching__c = '';} 
     if(l.flightPATH__c== null){l.flightPATH__c = '';}  
     if(l.ALB_Base_licence__c == null){l.ALB_Base_licence__c = '';}    
     if(l.Business_To_Business__c == null){l.Business_To_Business__c = '';} 
     if(l.Number_of_Temporary_Interfaces__c == null){l.Number_of_Temporary_Interfaces__c = '';} 
     if(l.Temporary_Interfaces_Duration__c == null){l.Temporary_Interfaces_Duration__c = '';} 
     if(l.Number_of_Temporary_Real_Servers__c == null){l.Number_of_Temporary_Real_Servers__c = '';} 
     if(l.Number_of_Temporary_jetSTREAM_Connection__c == null){l.Number_of_Temporary_jetSTREAM_Connection__c = '';} 
     if(l.Temporary_Interfaces_Duration__c == null){l.Temporary_Interfaces_Duration__c = '';} 
     if(l.Temporary_Real_Servers_Duration__c == null){l.Temporary_Real_Servers_Duration__c = '';} 
     if(l.jetSTREAM_Connections__c == null){l.jetSTREAM_Connections__c = '';} 
     //if(l.Temp_Throughput__c == null){l.Temp_Throughput__c = '';} 
     if(l.Temp_Throughput_Duration__c == null){l.Temp_Throughput_Duration__c = '';}  
     if(l.Throughput__c == null){l.Throughput__c = '';} 
     if(l.Throughput__c == 'Unlimited'){Throughput = '10000';} 
     if(l.Throughput__c == '500 Mbps'){Throughput = '5';} 
     if(l.Throughput__c == '1 Gbps'){Throughput = '10';} 
     if(l.Throughput__c == '2 Gbps'){Throughput = '20';} 
     if(l.Throughput__c == '3 Gbps'){Throughput = '30';} 
     if(l.Throughput__c == '4 Gbps'){Throughput = '40';} 
     if(l.Throughput__c == '5 Gbps'){Throughput = '50';} 
     if(l.Throughput__c == '6 Gbps'){Throughput = '60';} 
     if(l.Throughput__c == '10 Gbps'){Throughput = '100';} 
     if(l.Throughput__c == '20 Gbps'){Throughput = '200';}
     if(l.Throughput__c == '20 Gbps'){Throughput = '200';} 
     if(l.Throughput__c == '40 Gbps'){Throughput = '400';} 
     if(l.Throughput__c == '50 Gbps'){Throughput = '500';} 
     if(l.Throughput__c == '60 Gbps'){Throughput = '600';} 
     if(l.Throughput__c == '100 Gbps'){Throughput = '1000';} 
     
     if(l.Temp_Throughput__c == null){l.Temp_Throughput__c = '';} 
     if(l.Temp_Throughput__c == 'Unlimited'){Temp_Throughput = '10000';} 
     if(l.Temp_Throughput__c == '500 Mbps'){Temp_Throughput= '5';} 
     if(l.Temp_Throughput__c == '1 Gbps'){Temp_Throughput= '10';} 
     if(l.Temp_Throughput__c == '2 Gbps'){Temp_Throughput= '20';} 
     if(l.Temp_Throughput__c == '3 Gbps'){Temp_Throughput= '30';} 
     if(l.Temp_Throughput__c == '4 Gbps'){Temp_Throughput= '40';} 
     if(l.Temp_Throughput__c == '5 Gbps'){Temp_Throughput= '50';} 
     if(l.Temp_Throughput__c == '6 Gbps'){Temp_Throughput= '60';} 
     if(l.Temp_Throughput__c == '10 Gbps'){Temp_Throughput= '100';} 
     if(l.Temp_Throughput__c == '20 Gbps'){Temp_Throughput= '200';}
     if(l.Temp_Throughput__c == '20 Gbps'){Temp_Throughput= '200';} 
     if(l.Temp_Throughput__c == '40 Gbps'){Temp_Throughput= '400';} 
     if(l.Temp_Throughput__c == '50 Gbps'){Temp_Throughput= '500';} 
     if(l.Temp_Throughput__c == '60 Gbps'){Temp_Throughput= '600';} 
     if(l.Temp_Throughput__c == '100 Gbps'){Temp_Throughput= '1000';} 
     
     
       
     if(l.Temporary_jetSTREAM_Connections_Duration__c == null)
     {
       l.Temporary_jetSTREAM_Connections_Duration__c = '';
     }
     if(l.Contact_Telephone__c == null)
     {
       l.Contact_Telephone__c = '';
     }                       
    
    
     string fi ='';
     string frs ='';
     string fjc ='';
     string thp = '';
     if(l.Full_Interfaces__c == True) fi = 'on'; 
     if(l.Full_Real_Servers__c == True) frs = 'on'; 
     if(l.Full_jetSTREAM_Connections__c == True) fjc = 'on';
     if(l.Full_Throughput__c == True) thp = 'on';
     if((l.Full_Interfaces__c == True) || (l.Full_Real_Servers__c == True) || (l.Full_jetSTREAM_Connections__c == True))  
     {                      
     mbody = 'txteMAC='+l.Machine_ID__c+'&txteCustomer='+l.Customer_Name__r.Name+'&txteContact='+l.Contact_Person__r.Name+
                    '&txteTel='+l.Contact_Telephone__c+'&txteServerRef='+l.Server_Ref__c+'&txteIPsF='+l.Interfaces__c+
                    '&txteCSF='+l.Real_Servers__c+'&txteJSCF='+l.jetSTREAM_Connections__c+'&txteSSLtype='+l.Secure_Socket__c+
                    '&txteCacheType='+l.HTTP_Caching__c+'&txtePathType='+l.flightPATH__c+'&txteAccelType='+l.ALB_Base_licence__c+
                    '&txteB2Btype='+l.Business_To_Business__c+'&secret='+'1LoveJetnexus'+'&chkeIPsF='+fi+'&chkeCSF='+frs+
                    '&chkeJSCF='+fjc+'&chkeeTHF='+thp+'&eGLBtype='+l.Global_server_load_balancing__c+'&eAuthtype='+l.Pre_Authentication__c+
                    '&eFirewalltype='+l.Application_Firewall__c+'&eL4Btype='+l.Layer4__c+'&eL7Btype='+l.Layer7__c+'&eTHF='+Throughput;
     } 
    else{ 
     
            
     mbody = 'txteMAC='+l.Machine_ID__c+'&txteCustomer='+l.Customer_Name__r.Name+'&txteContact='+l.Contact_Person__r.Name+
                    '&txteServerRef='+l.Server_Ref__c+'&txteSSLtype='+l.Secure_Socket__c+
                    '&txteCacheType='+l.HTTP_Caching__c+'&txtePathType='+l.flightPATH__c+'&txteAccelType='+l.ALB_Base_licence__c+
                    '&txteB2Btype='+l.Business_To_Business__c+'&txteIPsType='+l.Temporary_Interfaces_Duration__c+'&txteIPsTimed='+
                    l.Number_of_Temporary_Interfaces__c+'&txteCSType='+l.Temporary_Real_Servers_Duration__c+'&txteCSTimed='+
                    l.Number_of_Temporary_Real_Servers__c+'&secret='+'1LoveJetnexus'+'&eGLBtype='+l.Global_server_load_balancing__c+
                    '&eAuthtype='+l.Pre_Authentication__c+'&eFirewalltype='+l.Application_Firewall__c+
                    '&eL4Btype='+l.Layer4__c+'&eL7Btype='+l.Layer7__c+'&eTHTimed='+Temp_Throughput+'&eTHtype='+l.Temp_Throughput_Duration__c;
     }               
     System.debug('mbody ='+ mbody);
    
     HttpRequest req = new HttpRequest();
     //req.setEndpoint('http://185.64.88.194:8888/');
     //req.setEndpoint('https://secure.jetnexus.com:8888/license.cgi');
     req.setEndpoint('https://secure.edgenexus.io:8888/license.cgi');   
     //https://secure.jetnexus.com:8888/
     req.setMethod('POST');
     req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
     req.setHeader('Host', 'secure.jetnexus.com');  
     req.setBody(mbody);
             
             
    //  if(!Test.isRunningTest()) {     
    Http http = new Http();
    HTTPResponse res = http.send(req);

    String  rbody = res.getBody();
    
    System.debug('Response:' + res.getBody());
    System.debug('Response in String:' + res.toString());
    System.debug('STATUS:'+res.getStatus());
    System.debug('STATUS_CODE:'+res.getStatusCode());
    
    
    String JSONContent = res.getBody();
    JSONParser parser =  JSON.createParser(JSONContent);

    //skip opening curly brace
    parser.nextToken();

    parser.nextValue();
    String fieldName = parser.getCurrentName();
    
    System.debug('Field Name1: ' + fieldName +' **** '+'Value 1: ' + parser.getText());
    if (fieldName == 'Lic_ALBExpiry') {
        Lic_ALBExpiry = parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
    }

    if (parser.getCurrentName()  == 'Lic_ALBPeriod')
    {  
        System.debug('Field Name3:' + parser.getCurrentName() +'****'+'Value 3:' + parser.getText());   
        Lic_ALBPeriod = parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_ApplicationExpiry')
    {     
        System.debug('Field Name4:' + parser.getCurrentName() +'****'+'Value 4:' + parser.getText());
        Lic_ApplicationExpiry = parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName() == 'Lic_ApplicationPeriod')
    {     
        System.debug('Field Name5:' + parser.getCurrentName() +'****'+'Value 5:' + parser.getText());
        Lic_ApplicationPeriod = parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_AuthorisationExpiry')
    {     
        System.debug('Field Name6:' + parser.getCurrentName() +'****'+'Value 6:' + parser.getText());
        Lic_AuthorisationExpiry= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_AuthorisationPeriod')
    {     
        System.debug('Field Name7:' + parser.getCurrentName() +'****'+'Value 7:' + parser.getText());
        Lic_AuthorisationPeriod= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
    
    }
    if (parser.getCurrentName()  == 'Lic_B2BExpiry')
    {     
        Lic_B2BExpiry= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_B2BPeriod')
    {     
        Lic_B2BPeriod= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    
    if (parser.getCurrentName()  == 'Lic_CacheExpiry')
    {     
        Lic_CacheExpiry= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_CachePeriod')
    {     
        Lic_CachePeriod= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
    
    }
    if (parser.getCurrentName() == 'Lic_CompanyId')
    {     
        Lic_CompanyId= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_ContactPerson')
    {     
        Lic_ContactPerson= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName() == 'Lic_CreatedBy')
    {     
        Lic_CreatedBy= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_CreatedDate')
    {     
        Lic_CreatedDate= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_GlobalExpiry')
    {     
        Lic_GlobalExpiry = parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_GlobalPeriod')
    {     
        Lic_GlobalPeriod= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_InterfacesExpiry')
    {     
        Lic_InterfacesExpiry= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_InterfacesNumber')
    {     
        Lic_InterfacesNumber= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_InterfacesPeriod')
    {     
        Lic_InterfacesPeriod= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_InterfacesFull')
    {     
        Lic_InterfacesFull= parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }
    if (parser.getCurrentName()  == 'Lic_IssuedDate')
    {
    Lic_IssuedDate = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_IssuedTo')
    {
    Lic_IssuedTo = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_LicenceID')
    {
    Lic_LicenceID = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_LicenceText')
    {
    Lic_LicenceText = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_MachineId')
    {
    Lic_MachineId = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_Name')
    {
    Lic_Name = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_RealServersExpiry')
    {
    Lic_RealServersExpiry = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_RealServersNumber')
    {
    Lic_RealServersNumber = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_RealServersPeriod')
    {
    Lic_RealServersPeriod = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_RealServersFull')
    {
    Lic_RealServersFull = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_ReviewDate')
    {
    Lic_ReviewDate = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_SSLExpiry')
    {
    Lic_SSLExpiry = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_SSLPeriod')
    {
    Lic_SSLPeriod = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_ServerRef')
    {
    Lic_ServerRef = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_Telephone')
    {
    Lic_Telephone = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_UpdatedBy')
    {
    Lic_UpdatedBy = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_UpdatedDate')
    {
    Lic_UpdatedDate = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_UserId')
    {
    Lic_UserId = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_Uuid')
    {
    Lic_Uuid = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_flightPathExpiry')
    {
    Lic_flightPathExpiry = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_flightPathPeriod')
    {
    Lic_flightPathPeriod = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_layer4Expiry')
    {
    Lic_layer4Expiry = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_layer4Period')
    {
    Lic_layer4Period = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if(parser.getCurrentName()  == 'Lic_layer7Expiry')
    {
    Lic_layer7Expiry = parser.getText();
    parser.nextToken();
    parser.nextValue();
    }
    if (parser.getCurrentName()  == 'Lic_layer7Period')
    {  
        System.debug('Field Name3:' + parser.getCurrentName() +'****'+'Value 3:' + parser.getText());   
        Lic_layer7Period = parser.getText(); 
        parser.nextToken() ; 
        parser.nextValue();
        
    }  
    
    Licencing__c ll = [Select Id,Machine_ID__c,Customer_Name__c,Contact_Person__c,Contact_Telephone__c,
                            Issue_Date__c,Server_Ref__c,ALB_Base_licence__c,Secure_Socket__c,HTTP_Caching__c,
                            flightPATH__c,Interfaces__c,Real_Servers__c,Full_Interfaces__c,Full_Real_Servers__c,
                            Business_To_Business__c,Temporary_Interfaces_Duration__c,Number_of_Temporary_Interfaces__c,
                            Temporary_Real_Servers_Duration__c,Number_of_Temporary_Real_Servers__c,
                            jetSTREAM_Connections__c,Full_jetSTREAM_Connections__c,Temporary_jetSTREAM_Connections_Duration__c,
                            Number_of_Temporary_jetSTREAM_Connection__c,ALB_Expiry__c,B2B_Expiry__c,Caching_Expiry__c,flightPATH_Expiry__c,
                            Interfaces_Expiry__c,jetSTREAM_Expiry__c,Licence_Expiry__c,Real_Servers_Expiry__c,SSL_Expiry__c,
                            Expiry_Date__c,Start_Date__c,Layer7_Expiry__c,Layer4_Expiry__c ,Application_Firewall_Expiry__c,
                            Global_Server_Load_Balancing_Expiry__c,Pre_Authentication_Expiry__c FROM Licencing__c where  ID =: LicId];
                            
    Licence_Key__c LK = new Licence_Key__c();
    LK.Licencing__c = ll.id;
    LK.Licence__c = Lic_LicenceText;
    Insert LK;

    ll.Lic_Uuid__c = Lic_Uuid;
    
    System.debug('Dates***' + Lic_B2BExpiry + '***' + Lic_ALBExpiry + '***' + Lic_CacheExpiry + '***' + Lic_flightPathExpiry);
    System.debug('Dates***' + Lic_InterfacesExpiry + '***' + Lic_ReviewDate + '***' + Lic_RealServersExpiry + '***' + Lic_SSLExpiry);
    DateParser dp = new DateParser();
    
    if(Lic_B2BExpiry != '' && Lic_B2BExpiry != null)
    {
        B2BExp = dp.ParseDate(Lic_B2BExpiry);
        System.debug('B2BExp Date***' + B2BExp);
        ll.B2B_Expiry__c = B2BExp;
    }  
    
    if(Lic_jetSTREAMExpiry != '' && Lic_jetSTREAMExpiry != null) 
    { 
        jetSTREAMExpiry= dp.ParseDate(Lic_jetSTREAMExpiry);
        System.debug('jetSTREAMExpiry Date***' + jetSTREAMExpiry);
        ll.jetSTREAM_Expiry__c = jetSTREAMExpiry;
    }

    if(Lic_ALBExpiry != '' && Lic_ALBExpiry != null) {
        System.debug('------------------');
        ALBExp = dp.ParseDate(Lic_ALBExpiry);
        System.debug('ALBExp Date***' + ALBExp);
        ll.ALB_Expiry__c = ALBExp;
    }

    if(Lic_CacheExpiry != '' && Lic_CacheExpiry != null) 
    { 
        CacheExp = dp.ParseDate(Lic_CacheExpiry);
        System.debug('CacheExp Date***' + CacheExp);
        ll.Caching_Expiry__c = CacheExp;
    }  
    if(Lic_flightPathExpiry != '' && Lic_flightPathExpiry !=null)  
    {
        flightPathExp = dp.ParseDate(Lic_flightPathExpiry);
        System.debug('flightPathExp Date***' + flightPathExp);
        ll.flightPATH_Expiry__c = flightPathExp;
    }  
    if(Lic_InterfacesExpiry != '' && Lic_InterfacesExpiry !=null)  
    {
        InterfacesExp = dp.ParseDate(Lic_InterfacesExpiry);
        System.debug('InterfacesExp Date' + InterfacesExp);
        ll.Interfaces_Expiry__c = InterfacesExp;
    }  
    if(Lic_ReviewDate != '') 
    { 
        if(Lic_ReviewDate != null)
        {
        if(Lic_ReviewDate != 'null')
        {
        LExpiryDate = dp.ParseDate(Lic_ReviewDate);
        System.debug('LExpiryDate Date' + LExpiryDate);
        ll.Licence_Expiry__c = LExpiryDate;
        }
        }
    }  
    if(Lic_RealServersExpiry != '' && Lic_RealServersExpiry !=null) 
    { 
        RealServersExp = dp.ParseDate(Lic_RealServersExpiry);
        System.debug('RealServersExp Date' + B2BExp);
        ll.Real_Servers_Expiry__c = RealServersExp;
    }  
    if(Lic_SSLExpiry != '' && Lic_SSLExpiry != null) 
    { 
        SSLExpiry = dp.ParseDate(Lic_SSLExpiry);
        System.debug('SSLExpiry Date' + SSLExpiry);
        ll.SSL_Expiry__c = SSLExpiry;
    //.Lic_B2BExpiry__c = B2BExp ; 
    }
    if(Lic_layer7Expiry != '' && Lic_layer7Expiry != null)
    {
        layer7Expiry = dp.ParseDate(Lic_layer7Expiry);
        System.debug('layer7Expiry  Date***' + layer7Expiry);
        ll.Layer7_Expiry__c = layer7Expiry;
    }
    if(Lic_layer4Expiry != '' && Lic_layer4Expiry != null)
    {
        layer4Expiry = dp.ParseDate(Lic_layer4Expiry);
        System.debug('layer4Expiry  Date***' + layer4Expiry);
        ll.Layer4_Expiry__c = layer4Expiry;
    }
    if(Lic_ApplicationExpiry != '' && Lic_ApplicationExpiry != null)
    {
        ApplicationExpiry = dp.ParseDate(Lic_ApplicationExpiry);
        System.debug('ApplicationExpiry Date***' + ApplicationExpiry);
        ll.Application_Firewall_Expiry__c = ApplicationExpiry;
    }
    if(Lic_GlobalExpiry != '' && Lic_GlobalExpiry != null)
    {
        GlobalExpiry= dp.ParseDate(Lic_GlobalExpiry);
        System.debug('GlobalExpiryDate***' + GlobalExpiry);
        ll.Global_Server_Load_Balancing_Expiry__c = GlobalExpiry;
    }
    if(Lic_AuthorisationExpiry!= '' && Lic_AuthorisationExpiry!= null)
    {
        AuthorisationExpiry= dp.ParseDate(Lic_AuthorisationExpiry);
        System.debug('AuthorisationExpiry Date***' + AuthorisationExpiry);
        ll.Pre_Authentication_Expiry__c = AuthorisationExpiry;
    }
    //ll.Server_Ref__c = Lic_ServerRef;
    ll.Issue_Date__c = System.today();
    //ll.Machine_ID__c = Lic_MachineId;  
    ll.Request_Status__c = 'Licence Recieved';
    ll.Status__c = 'Active';
    update ll;
    
       
    //  }
   }
 }