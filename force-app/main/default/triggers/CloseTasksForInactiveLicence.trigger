trigger CloseTasksForInactiveLicence on Licencing__c (after update) {
    Set<Id> licences=new Set<Id>();
    List<Task> updateTasks= new List<Task>();
    
    for(Licencing__c l : trigger.new)
    {
        if(l.status__c=='Inactive'){
            licences.add(l.Id);
        }   
    } 
    for(Task ta:[Select id,status,ActivityDate from task where whatId IN:licences ])
     {
         ta.status='Completed';
         ta.ActivityDate=system.today();
         updateTasks.add(ta);
     }
    
    update updateTasks;
}