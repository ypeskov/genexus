trigger LicenseAppCallout on Licencing__c (after Insert) {

  for(Licencing__c Li : trigger.new)
  {
    AuthCallout.basicAuthCallout(Li.Id);
  }

}